/* see https://json-ld.org/ & https://www.w3.org/TR/did-core/ */

export const did = {
  '@context': 'https://www.w3.org/ns/did/v1',
  '@id': 'did:example:123456789abcdefghi',
  authentication: [{
    // use to authenticated as did ...fghi
    '@id': 'did:example:123456789abcdefghi#keys:=1',
    type: 'CPTZkp',
    controller: 'did:example:123456789abcdefghi',
    publicKeyPem: '-----BEGIN PUBLIC KEY...END PUBLIC KEY-----\r\n',
  }],
  service: [{
  // used to retrieve Verifiable Credentials associated with the DID
    '@id': 'did:example:123456789abcdefghi#vcs',
    type: 'VerifiableCredentialService',
    serviceEndpoint: 'https://example.com/vc/'
  }]
}
