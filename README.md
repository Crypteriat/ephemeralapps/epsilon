# Epsilon

An implementation of an ephemeral link generator to be used with an Identity
Manager.  The link can either produce a progressive web app or issue links to be
consumed by web apps. This runs as a Deno service that can hosted and forms a
decentralized network for running ephemeral apps.

1. User Interaction
2. Browser Home Screen Installation and Security
3. Source Organization
3. Technology Stack
5. Composability
6. Royalties
7. License

## Introduction

This project is based on Deno. Our goal is to leverage functional programming
and smart contract integration to easily interface to a cross-chain smart contract
with a web interface. This link acts as the "home" or index page for the website.
The link may be installed on the home screen behaving similarly to a progressive web app (PWA).
Our intent is to provide an integration layer with Android & iOS. As many of the common
functions that are available via the browser will be provided. Thus, the resulting PWA
may have a subset of the services available to native applications.
The purpose of this project is solely to generate links for a smart contract "back-end."
See [test](test)

## Why Links instead of OAuth or Sockets?
1. Can be dynamic. They provide much better privacy and can resolve dynamically via DNS or to specific IP addresses.
2. Can be more easily self-signed to support TLS (e.g. HTTPS).
3. Can be immediately used within the browser. Scripting, testing, or immediate scrutiny of the link can be done in any web browser or via command line tools such as `curl`.
4. With Deno, can provide flexible and secure executables.
4. Can be redirected to other ephemeral links or sites.

### Application

* Create a nym for a link.

### Browser Home Screen Security

### Source Organization

### Technology Stack

[Decentralizied Identifiers](https://www.w3.org/TR/did-core/) and [ActivityPub](https://www.w3.org/TR/activitypub/) compatible links are key
components of the Epsilon generator. The ephemeral links are designed to  conform with these emerging standards.

### Composability

### Royalties

### License


